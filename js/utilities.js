/**
 * Javascript implementation of String.format or printf
 *
 * example of usage
 * 'vincenty lat1: {0}  lon1: {1}  lat2: {2}  lon2: {3}'.format(lat1, lon1, lat2, lon2)
 *
 * outputs
 * vincenty lat1: 40.689247  lon1: -74.044502  lat2: 40.7747222222  lon2: -73.8719444444
 *
 */
if (typeof String.prototype.format === "undefined") { // make sure it's not implemented
  String.prototype.format = function () {
    "use strict";
    var args = arguments;
    return this.replace(/\{(\d+)\}/g, function (match, number) {
      return typeof args[number] !== 'undefined' ? args[number] : match;
    });
  };
}

/**
 * Deep recursive merge of object2 into object1
 * @param {Object} object1
 * @param {Object} object2
 * return {Object}
 */
function mergeObjects (object1, object2) {
  for (var property in object2) {
    if (object1.hasOwnProperty(property) &&
        object2.propertyIsEnumerable(property) &&
        typeof object2[property] === "object") {

      mergeObjects(object1[property], object2[property]);

    } else {
      object1[property] = object2[property];
    }
  }

  return object1;
}
